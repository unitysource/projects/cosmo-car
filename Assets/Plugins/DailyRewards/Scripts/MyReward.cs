﻿/***************************************************************************\
Project:      Daily Rewards
Copyright (c) Niobium Studios.
Author:       Guilherme Nunes Barbosa (gnunesb@gmail.com)
\***************************************************************************/
using UnityEngine;
using System;
using UnityEngine.Serialization;

namespace NiobiumStudios
{
    /**
    * The class representation of the Reward
    **/
    [Serializable]
    public class MyReward
    {
        public string myUnit;
        public int myReward;
        public Sprite mySprite;
    }
}