	
	////////////////////////////////////////
    //    Shared Editor Tool Utilities    //
	//    by KrisDevelopment              //
    ////////////////////////////////////////
    
		SETUtil is an open-source project,
	publicly available free of charge.
		The purpose of SETUtil is to act as a hub,
    isolating serialization weak points
    and providing common utilities,
    to allow for more streamlined editor tool development.
    
	GitLab: https://gitlab.com/KrisDevelopment/SETUtil