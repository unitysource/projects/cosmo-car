﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
///     UI input (float) receiver from UI Button.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/UI/Mobile/RCC UI Controller Button")]
public class RCC_UIController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool pressing;

    private Button button;

    private Physics handBrake;

    internal float input;
    private Slider slider;
    private float sensitivity => RCCSettings.UIButtonSensitivity;
    private float gravity => RCCSettings.UIButtonGravity;

    private void Awake()
    {
        button = GetComponent<Button>();
        slider = GetComponent<Slider>();
    }

    private void Update()
    {
        if (button && !button.interactable)
        {
            pressing = false;
            input = 0f;
            return;
        }

        if (slider && !slider.interactable)
        {
            pressing = false;
            input = 0f;
            slider.value = 0f;
            return;
        }

        if (slider)
        {
            if (pressing)
                input = slider.value;
            else
                input = 0f;

            slider.value = input;
        }
        else
        {
            if (pressing)
                input += Time.deltaTime * sensitivity;
            else
                input -= Time.deltaTime * gravity;
        }

        if (input < 0f)
            input = 0f;

        if (input > 1f)
            input = 1f;
    }

    private void OnDisable()
    {
        input = 0f;
        pressing = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pressing = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressing = false;
    }

    private void OnPress(bool isPressed)
    {
        if (isPressed)
            pressing = true;
        else
            pressing = false;
    }

    // Getting an Instance of Main Shared RCC Settings.

    #region RCC Settings Instance

    private RCC_Settings RCCSettingsInstance;

    private RCC_Settings RCCSettings
    {
        get
        {
            if (RCCSettingsInstance == null) RCCSettingsInstance = RCC_Settings.Instance;
            return RCCSettingsInstance;
        }
    }

    #endregion
}