﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AllMenuManager : MonoBehaviour
{
    private const string NewB = "" + ""; //+
    private static AllMenuManager _inst; //
    public static bool IsPitchP, IsPitchP2, Pitch; //+

    public GameObject headP; //+

    public GameObject entryP; //+

    public GameObject entryP2; //+

    public GameObject modP; //+

public GameObject waitP; //+
public GameObject outP; //+
 public Text coinsText; //+
public Button[] levelB; //+

public Button[] lB2; //+

    //	public AudioClip clickSound;
    // bool newHeyzaponce;//+
    // GameObject _cam;//+
 public Slider musicSlider; //+
public GameObject[] icons; //+

    private void Awake()
    {
        if (_inst == null)
            _inst = this;
        else
            Destroy(this);
    }


    private void Start()
    {
        if (IsPitchP || IsPitchP2)
            InvokeRepeating("ChangeAd", 1.0f, 2f);


        CheckLevel();
        Check2Mode();

        if (IsPitchP)
        {
            entryP.SetActive(true);
            headP.SetActive(false);
            IsPitchP = false;
        }


        if (IsPitchP2)
        {
            entryP2.SetActive(true);
            headP.SetActive(false);
            IsPitchP2 = false;
        }


        Time.timeScale = 1;

        coinsText.text = PlayerPrefs.GetInt("Earnings").ToString();

        if (PlayerPrefs.HasKey("Sound"))
            musicSlider.value = PlayerPrefs.GetFloat("Sound");
        else
            musicSlider.value = 1f;

        Upgrade(PlayerPrefs.GetInt("Controls"));

        // _cam = GameObject.FindGameObjectWithTag ("MainCamera");

        LoadLevels.LVar = 1;

        PlayerPrefs.SetInt("Car_crash", 0);
        PlayerPrefs.SetInt("Bluecar", 1);
        PlayerPrefs.SetInt("Whitecar", 0);
        PlayerPrefs.SetInt("Pinkcar", 0);
        PlayerPrefs.SetInt("Yellowcar", 0);
        PlayerPrefs.SetInt("Level1", 1);
        PlayerPrefs.SetInt("Mode2Level1", 1);

        // a = PlayerPrefs.GetInt ("coins");


        PurchCheck();
        Invoke("ShowAdsAtStart", 0.1f);
    }


    private void Update()
    {
        AudioListener.volume = musicSlider.value;
        PlayerPrefs.SetFloat("Sound", musicSlider.value);
    }


    public void OnGoOut()
    {
        outP.SetActive(true);
    }


    public void OnQuit()
    {
        Application.Quit();
    }

    public void OnContinue()
    {
        outP.SetActive(false);
    }


    private void PurchCheck()
    {
        if (PlayerPrefs.GetInt("UNLOCKLEVELS") == 1)
            foreach (var LB2 in lB2)
                LB2.interactable = true;

        if (PlayerPrefs.GetInt("UNLOCKLEVELS") == 1)
            foreach (var LB1 in levelB)
                LB1.interactable = true;
    }

    public void TextShare()
    {
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID
        //Refernece of AndroidJavaClass class for intent
        var intentClass = new AndroidJavaClass("android.content.Intent");
        //Refernece of AndroidJavaObject class for intent
        var intentObject = new AndroidJavaObject("android.content.Intent");
        //call setAction method of the Intent object created
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        //set the type of sharing that is happening
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");
        //add data to be passed to the other activity i.e., the data to be sent
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), NewB);
        //get the current activity
        var unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        //start the activity by sending the intent data
        currentActivity.Call("startActivity", intentObject);
#endif
    }

    public void OnBack()
    {
        entryP.SetActive(false);
        entryP2.SetActive(false);
        modP.SetActive(true);
    }


    public void OnPlay()
    {
        headP.SetActive(false);
        modP.SetActive(true);
    }


    public void OnMenu()
    {
        headP.SetActive(true);
        modP.SetActive(false);
    }


    public void CloseIcnBtn()
    {
        headP.SetActive(true);
    }


    public void Administration()
    {
        PlayerPrefs.SetInt("Controls", 2);
        Upgrade(2);
    }

    public void OnAcc()
    {
        PlayerPrefs.SetInt("Controls", 1);
        Upgrade(1);
    }

    public void OnBtn()
    {
        PlayerPrefs.SetInt("Controls", 0);
        Upgrade(0);
    }

    private void Upgrade(int val)
    {
        switch (val)
        {
            case 0:
                icons[0].SetActive(true);
                icons[1].SetActive(false);
                icons[2].SetActive(false);
                break;
            case 1:
                icons[0].SetActive(false);
                icons[1].SetActive(true);
                icons[2].SetActive(false);
                break;
            case 2:
                icons[0].SetActive(false);
                icons[1].SetActive(false);
                icons[2].SetActive(true);
                break;
        }
    }

    private void CheckLevel()
    {
        PlayerPrefs.SetInt("Level1", 1);


        // var level = PlayerPrefs.GetInt("LevelNumber");


        if (PlayerPrefs.GetInt("Level2") == 1) levelB[0].interactable = true;
        if (PlayerPrefs.GetInt("Level3") == 1) levelB[1].interactable = true;
        if (PlayerPrefs.GetInt("Level4") == 1) levelB[2].interactable = true;
        if (PlayerPrefs.GetInt("Level5") == 1) levelB[3].interactable = true;
        // if (PlayerPrefs.GetInt("Level6") == 1) levelB[4].interactable = true;
        // if (PlayerPrefs.GetInt("Level7") == 1) levelB[5].interactable = true;
        // if (PlayerPrefs.GetInt("Level8") == 1) levelB[6].interactable = true;
        // if (PlayerPrefs.GetInt("Level9") == 1) levelB[7].interactable = true;
        // if (PlayerPrefs.GetInt("Level10") == 1) levelB[8].interactable = true;
    }

    private void Check2Mode()
    {
        PlayerPrefs.SetInt("Mode2Level1", 1);


        // var Mode2level = PlayerPrefs.GetInt("Mode2LevelNumber");


        if (PlayerPrefs.GetInt("Mode2Level2") == 1) lB2[0].interactable = true;
        if (PlayerPrefs.GetInt("Mode2Level3") == 1) lB2[1].interactable = true;
        if (PlayerPrefs.GetInt("Mode2Level4") == 1) lB2[2].interactable = true;
        if (PlayerPrefs.GetInt("Mode2Level5") == 1) lB2[3].interactable = true;
        // if (PlayerPrefs.GetInt("Mode2Level6") == 1) lB2[4].interactable = true;
        // if (PlayerPrefs.GetInt("Mode2Level7") == 1) lB2[5].interactable = true;
        // if (PlayerPrefs.GetInt("Mode2Level8") == 1) lB2[6].interactable = true;
        // if (PlayerPrefs.GetInt("Mode2Level9") == 1) lB2[7].interactable = true;
        // if (PlayerPrefs.GetInt("Mode2Level10") == 1) lB2[8].interactable = true;
    }

    public void OnLevel1()
    {
        PlayerPrefs.SetInt("LevelNumber", 1);
        PlayerPrefs.SetInt("AnalyticsNo", 1);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnLevel2()
    {
        PlayerPrefs.SetInt("LevelNumber", 2);
        PlayerPrefs.SetInt("AnalyticsNo", 2);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnLevel3()
    {
        PlayerPrefs.SetInt("LevelNumber", 3);
        PlayerPrefs.SetInt("AnalyticsNo", 3);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnLevel4()
    {
        PlayerPrefs.SetInt("LevelNumber", 4);
        PlayerPrefs.SetInt("AnalyticsNo", 4);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnLevel5()
    {
        PlayerPrefs.SetInt("LevelNumber", 5);
        PlayerPrefs.SetInt("AnalyticsNo", 5);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }
    
    public void OnHard1()
    {
        PlayerPrefs.SetInt("Mode2LevelNumber", 1);
        PlayerPrefs.SetInt("AnalyticsNo", 11);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnHard2()
    {
        PlayerPrefs.SetInt("Mode2LevelNumber", 2);
        PlayerPrefs.SetInt("AnalyticsNo", 12);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnHard3()
    {
        PlayerPrefs.SetInt("Mode2LevelNumber", 3);
        PlayerPrefs.SetInt("AnalyticsNo", 13);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnHard4()
    {
        PlayerPrefs.SetInt("Mode2LevelNumber", 4);
        PlayerPrefs.SetInt("AnalyticsNo", 14);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnHard5()
    {
        PlayerPrefs.SetInt("Mode2LevelNumber", 5);
        PlayerPrefs.SetInt("AnalyticsNo", 15);
        waitP.SetActive(true);
        SceneManager.LoadSceneAsync(2);
    }

    public void OnModeFirst()
    {
        entryP.SetActive(true);
        modP.SetActive(false);
        PlayerPrefs.SetInt("LevelNumber", 0);
        PlayerPrefs.SetInt("Mode2LevelNumber", 0);
        if (PlayerPrefs.GetInt("UNLOCKLEVELS") == 1)
            foreach (var LB1 in levelB)
                LB1.interactable = true;
        Pitch = true;
    }

    public void OnModeSecond()
    {
        entryP2.SetActive(true);
        modP.SetActive(false);

        PlayerPrefs.SetInt("LevelNumber", 0);
        PlayerPrefs.SetInt("Mode2LevelNumber", 0);
        if (PlayerPrefs.GetInt("UNLOCKLEVELS") == 1)
            foreach (var LB2 in lB2)
                LB2.interactable = true;
        Pitch = false;
    }
}