﻿using UnityEngine;
using UnityEngine.Serialization;

public class AmountOfFrames : MonoBehaviour
{
    public int val;

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
    }

    private void Update()
    {
        if (Application.targetFrameRate != val)
            Application.targetFrameRate = val;
    }
}