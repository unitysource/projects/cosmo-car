﻿using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
public Transform player;

public float temp;

    private void Start() => 
        Time.timeScale = 1f;

    private void Update() => 
        transform.RotateAround(player.position, Vector3.up, 10 * temp * Time.deltaTime);
}