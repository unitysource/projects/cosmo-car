using UnityEngine;

public class CarCheck : MonoBehaviour
{
    public void OnCollisionEnter(Collision col)
    {
        GetComponent<Collider>().enabled = !col.gameObject.CompareTag("Hero");
    }
}