﻿using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    private static float _remainingClock = 300.0f;
    private static bool _isPause = true;

public float goneClock;

public Text txt;

    // private bool _ort;
    // public GameObject panelIsLost;
    // public GameObject isCarTrue;

    private float _min;
    private float _sec;


    public void Start()
    {
        // isCarTrue = GameObject.FindGameObjectWithTag("Hero");
        // _ort = true;
        TimerBegin(goneClock);
    }

    private void Update()
    {
        _remainingClock -= Time.deltaTime;

        _min = Mathf.Floor(_remainingClock / 60);
        _sec = _remainingClock % 60;
        if (_sec > 59) _sec = 59;
        if (_min < 0)
        {
            _isPause = true;
            _min = 0;
            _sec = 0;
        }
    }

    private void TimerBegin(float from)
    {
        _isPause = false;
        _remainingClock = from;
        StartCoroutine(CoroutineAllUpdate());
    }

    private IEnumerator CoroutineAllUpdate()
    {
        while (!_isPause)
        {
            txt.text = $"{_min:0}:{_sec:00}";
            yield return new WaitForSeconds(0.2f);
        }
    }
}