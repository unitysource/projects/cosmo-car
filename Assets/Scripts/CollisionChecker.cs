﻿using Tools;
using UnityEngine;

public class CollisionChecker : MonoBehaviour
{
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag(Tags.PlaneCountLives)) 
            gameObject.SetActive(false);
    }
}