﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public Text countdownText;

    private string _m2;
    private string _min2;
    private float _startT;

    private void Start()
    {
        Debug.Log(PlayerPrefs.GetInt("Minutes") + " : " + PlayerPrefs.GetInt("Seconds"));
        countdownText.text = PlayerPrefs.GetInt("Minutes") + " : " + PlayerPrefs.GetInt("Seconds");
        _startT = Time.time;
        Debug.Log("start time : " + _startT);
    }


    private void Update()
    {
        var tm = Time.time - _startT;
        PlayerPrefs.SetInt("Minutes", (int) tm / 60);
        PlayerPrefs.SetInt("Seconds", (int) tm % 60);

        countdownText.text = PlayerPrefs.GetInt("Minutes") + " : " + PlayerPrefs.GetInt("Seconds");

        PlayerPrefs.SetInt("Seconds", Convert.ToInt32(_m2));
        PlayerPrefs.SetInt("Minutes", Convert.ToInt32(_min2));
    }
}