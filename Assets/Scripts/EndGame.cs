﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    // private static int AdCount = 0;

    public static int MoneyGiveUp;

    // public GameObject winPanel;

    //public Text timer;
    //public Text crash;
     public Text rewText;
    public Text moneyTxt;
    public Text allPoints;
    // public float minutes;
    public GameObject[] player;
    public GameObject[] endGameGO;
    public GameObject[] end;
    private int car_crash;
    private int _allPoints;

//	public Image timer_yellowbar;
//	public Image crash_yellowbar;
//	public Image reward_yellowbar;
    // private bool _isGO = false;
    // private bool _onceAd;
    // private float reward_division;
    // private float seconds;

    public void Start()
    {
        //	level_Events("Start");
        //	car = GameObject.FindGameObjectWithTag ("Player");
        // _onceAd = true;
        MoneyGiveUp = 0;
        PlayerPrefs.SetInt("current", 0);
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (gameObject.CompareTag("FinishedCall") && collider.CompareTag("Hero"))
        {
            var level = PlayerPrefs.GetInt("LevelNumber");
            var mode2Level = PlayerPrefs.GetInt("Mode2LevelNumber");
            foreach (var CC in player) CC.GetComponent<Rigidbody>().isKinematic = enabled;
            foreach (var EOf in end)
            {
                if(EOf != null)
                    EOf.SetActive(false);
            }
            endGameGO[0].SetActive(true);
            endGameGO[1].SetActive(true);
            Invoke(nameof(Off1), 0.4f);
            switch (level)
            {
                case 1:
                    PlayerPrefs.SetInt("current", 500);
                    PlayerPrefs.SetInt("Level2", 1);
                    break;
                case 2:
                    PlayerPrefs.SetInt("current", 1000);
//				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 1000);
                    PlayerPrefs.SetInt("Level3", 1);
                    break;
                case 3:
                    //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 1500);
                    PlayerPrefs.SetInt("Level4", 1);
                    PlayerPrefs.SetInt("current", 1500);
                    break;
                case 4:
                    PlayerPrefs.SetInt("Level5", 1);
//				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 2000);
                    PlayerPrefs.SetInt("current", 2000);
                    break;
                // case 5:
                //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 2500);
                //     PlayerPrefs.SetInt("Level6", 1);
                //     PlayerPrefs.SetInt("current", 2500);
                //     break;
            }

            //             if (level == 6)
//             {
// //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 3000);
//                 PlayerPrefs.SetInt("Level7", 1);
//                 PlayerPrefs.SetInt("current", 3000);
//             }
//
//             if (level == 7)
//             {
// //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 3500);
//                 PlayerPrefs.SetInt("Level8", 1);
//                 PlayerPrefs.SetInt("current", 3500);
//             }
//
//             if (level == 8)
//             {
// //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 4000);
//                 PlayerPrefs.SetInt("Level9", 1);
//                 PlayerPrefs.SetInt("current", 4000);
//             }
//
//             if (level == 9)
//             {
// //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 4500);
//                 PlayerPrefs.SetInt("Level10", 1);
//                 PlayerPrefs.SetInt("current", 4500);
//             }
//
//             if (level == 10)
//             {
// //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 5000);
//                 PlayerPrefs.SetInt("Level11", 1);
//                 PlayerPrefs.SetInt("current", 5000);
//             }

            switch (mode2Level)
            {
                case 1:
                    PlayerPrefs.SetInt("current", 500);
                    PlayerPrefs.SetInt("Mode2Level2", 1);
                    break;
                case 2:
                    PlayerPrefs.SetInt("current", 1000);
                    //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 1000);
                    PlayerPrefs.SetInt("Mode2Level3", 1);
                    break;
                case 3:
                    //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 1500);
                    PlayerPrefs.SetInt("Mode2Level4", 1);
                    PlayerPrefs.SetInt("current", 1500);
                    break;
                case 4:
                    PlayerPrefs.SetInt("Mode2Level5", 1);
                    //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 2000);
                    PlayerPrefs.SetInt("current", 2000);
                    break;
                // case 5:
                //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 2500);
                //     PlayerPrefs.SetInt("Mode2Level6", 1);
                //     PlayerPrefs.SetInt("current", 2500);
                //     break;
            }

            //
            // if (mode2Level == 6)
            // {
            //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 3000);
            //     PlayerPrefs.SetInt("Mode2Level7", 1);
            //     PlayerPrefs.SetInt("current", 3000);
            // }
            //
            // if (mode2Level == 7)
            // {
            //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 3500);
            //     PlayerPrefs.SetInt("Mode2Level8", 1);
            //     PlayerPrefs.SetInt("current", 3500);
            // }
            //
            // if (mode2Level == 8)
            // {
            //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 4000);
            //     PlayerPrefs.SetInt("Mode2Level9", 1);
            //     PlayerPrefs.SetInt("current", 4000);
            // }
            //
            // if (mode2Level == 9)
            // {
            //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 4500);
            //     PlayerPrefs.SetInt("Mode2Level10", 1);
            //     PlayerPrefs.SetInt("current", 4500);
            // }
            //
            // if (mode2Level == 10)
            // {
            //     //				PlayerPrefs.SetInt ("Earnings", PlayerPrefs.GetInt("Earnings") + 5000);
            //     PlayerPrefs.SetInt("Mode2Level11", 1);
            //     PlayerPrefs.SetInt("current", 5000);
            // }

            Levels.IsTime = false;
            _allPoints = MoneyGiveUp * 50;
            moneyTxt.text = $"{MoneyGiveUp} X 50 = {_allPoints}";
            var tt = PlayerPrefs.GetInt("current") + _allPoints;
            allPoints.text = tt.ToString();


            //Ads_Manager.Instance.ShowLargeAdmobBanner ();


            Invoke(nameof(Fin), 2f);
        }
    }

    private void Off1()
    {
        endGameGO[2].SetActive(true);
        Invoke(nameof(Off2), 0.4f);
    }

    private void Off2()
    {
        endGameGO[3].SetActive(true);
        Invoke(nameof(Off3), 0.4f);
    }

    private void Off3()
    {
        endGameGO[4].SetActive(true);
    }


    private void Fin()
    {
        //	level_Events("Complete");
        car_crash = PlayerPrefs.GetInt("Car_crash");
        //crash.text = car_crash.ToString ();
        //timer.text = seconds.ToString (); 
        PlayerPrefs.SetInt("Car_crash", 0);


        float earning = PlayerPrefs.GetInt("current");

//		print (earning);
        rewText.text = "" + (int) earning;
        PlayerPrefs.SetInt("Earnings", PlayerPrefs.GetInt("Earnings") + (int) earning + _allPoints);
        Levels.Inst.winCanv.SetActive(true);
    }
}