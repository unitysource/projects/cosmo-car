using System;
using Tools;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static event Action<Collision> OnDeath;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(Tags.Hero))
        {
            Debug.Log("HERE");
            OnDeath?.Invoke(other);
        }
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.CompareTag(Tags.Hero))
    //     {
    //         Debug.Log("HERE");
    //         OnDeath?.Invoke(other);
    //         // Time.timeScale = 0;
    //     }
    // }
}