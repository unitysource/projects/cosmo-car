﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GroundFall : MonoBehaviour
{
    public static int AdmirationCount;
    public static int SpawnCount;

    public Text crashText;

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;

    public Animator anim;
    public Animator anim2;

    private Transform[] _spawnPointsCheck;

    private void OnEnable() => 
        Enemy.OnDeath += Death;

    private void OnDisable() => 
        Enemy.OnDeath -= Death;

    private void Start()
    {
        var spContainer = GameObject.FindGameObjectWithTag("SpawnPointContainer").transform;
        _spawnPointsCheck = new Transform[spContainer.childCount];

        for (int i = 0; i < spContainer.childCount; i++)
            _spawnPointsCheck[i] = spContainer.GetChild(i);

        SpawnCount = 0;
        OutPos();
    }

    private void OnTriggerEnter(Collider other)
    {
        Death(other);
    }

    private void OnCollisionEnter(Collision other)
    {
        Death(other);
    }

    private void Death(Collider collision)
    {
        anim.enabled = false;
        anim2.enabled = false;

        if (collision.gameObject.CompareTag("Hero"))
        {
            player1.GetComponent<Rigidbody>().isKinematic = enabled;
            player2.GetComponent<Rigidbody>().isKinematic = enabled;
            player3.GetComponent<Rigidbody>().isKinematic = enabled;
            player4.GetComponent<Rigidbody>().isKinematic = enabled;

            AdmirationCount++;
            // Time.timeScale = 1f;
            if (AdmirationCount != 3) Invoke(nameof(OutPos), .1f);

            crashText.text = (3 - AdmirationCount).ToString();

            if (AdmirationCount == 3)
            {
                Levels.Inst.Fail2();
                AdmirationCount = 0;
            }
        }
    }
    
    private void Death(Collision collision)
    {
        anim.enabled = false;
        anim2.enabled = false;

        if (collision.gameObject.CompareTag("Hero"))
        {
            player1.GetComponent<Rigidbody>().isKinematic = enabled;
            player2.GetComponent<Rigidbody>().isKinematic = enabled;
            player3.GetComponent<Rigidbody>().isKinematic = enabled;
            player4.GetComponent<Rigidbody>().isKinematic = enabled;

            AdmirationCount++;
            // Time.timeScale = 1f;
            if (AdmirationCount != 3) Invoke(nameof(OutPos), .1f);

            crashText.text = (3 - AdmirationCount).ToString();

            if (AdmirationCount == 3)
            {
                Levels.Inst.Fail2();
                AdmirationCount = 0;
            }
        }
    }

    private void OutPos()
    {
        anim.enabled = true;
        anim2.enabled = true;

        player1.transform.position = _spawnPointsCheck[SpawnCount].transform.position;
        player1.transform.rotation = _spawnPointsCheck[SpawnCount].transform.rotation;
        player2.transform.position = _spawnPointsCheck[SpawnCount].transform.position;
        player2.transform.rotation = _spawnPointsCheck[SpawnCount].transform.rotation;
        player3.transform.position = _spawnPointsCheck[SpawnCount].transform.position;
        player3.transform.rotation = _spawnPointsCheck[SpawnCount].transform.rotation;
        player4.transform.position = _spawnPointsCheck[SpawnCount].transform.position;
        player4.transform.rotation = _spawnPointsCheck[SpawnCount].transform.rotation;

        player1.GetComponent<Rigidbody>().isKinematic = !enabled;
        player2.GetComponent<Rigidbody>().isKinematic = !enabled;
        player3.GetComponent<Rigidbody>().isKinematic = !enabled;
        player4.GetComponent<Rigidbody>().isKinematic = !enabled;


        Levels.EndTimer += 5f;
    }
}