﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Levels : MonoBehaviour
{
    public static Levels Inst;
    public static float EndTimer = 300.0f;
    public static bool IsTime = true;
    public GameObject stopCanv;
    public GameObject winCanv;
    public GameObject gameOverCanv;

    [FormerlySerializedAs("ourCar")] public GameObject[] players;

    // public GameObject rCcCamera;
    public GameObject rccCanv;
    public float startTime;
    public Text countdownText;
    public GameObject[] frameIcon;

    public bool isOn;
    // private bool _o;
    
    private RCC_MobileButtons _control;
    private int _controlAmount;
    private int counterCamera;
    // private Coroutine cour;

    private bool _gOver;
    private bool _fail = false;
    private bool isPaused;
    private bool isF = false;

    private GameObject _selPlayer;
    private bool _isEnd;
    private float _min;
    private float _sec;
    private float tS;

    private void Awake()
    {
        if (Inst != null)
            Destroy(gameObject);
        else
            Inst = this;
        IsTime = true;
    }

    private void Start()
    {
        _gOver = true;
        // isPause = false;
        var levelsChk = PlayerPrefs.GetInt("AnalyticsNo");
        _control = rccCanv.GetComponent<RCC_MobileButtons>();
        AudioListener.volume = PlayerPrefs.GetFloat("Sound");
//        print(AudioListener.volume);
        if (PlayerPrefs.GetInt("Controls") == 1)
        {
            _control.SetMobileController(1);
        }
        else if (PlayerPrefs.GetInt("Controls") == 2)
        {
            _controlAmount = 2;
            _control.SetMobileController(2);
        }
        else if (PlayerPrefs.GetInt("Controls") == 0)
        {
            _control.SetMobileController(0);

            _controlAmount = 0;
        }

        UpdateController(PlayerPrefs.GetInt("Controls"));
        Time.timeScale = 1;
        TimerBegin(startTime);


        countdownText.text = PlayerPrefs.GetInt("Minutes") + " : " + PlayerPrefs.GetInt("Seconds");
        tS = Time.time;
        //		Debug.Log ("start time : " + Starttime);


        PlayerPrefs.SetInt("Car_crash", 0);
        counterCamera = 1;

        switch (SelectCarManager.SelPlayers)
        {
            case 0:
                players[0].SetActive(true);
                players[1].SetActive(false);
                players[2].SetActive(false);
                players[3].SetActive(false);
                _selPlayer = players[0];
//			cars [4].SetActive (false);
                break;
            case 1:
                players[0].SetActive(false);
                players[1].SetActive(true);
                players[2].SetActive(false);
                players[3].SetActive(false);
                _selPlayer = players[1];
//			cars [4].SetActive (false);
                break;
            case 2:
                players[0].SetActive(false);
                players[1].SetActive(false);
                players[2].SetActive(true);
                players[3].SetActive(false);
                _selPlayer = players[2];
                //			cars [4].SetActive(false);
                break;
            case 3:
                players[0].SetActive(false);
                players[1].SetActive(false);
                players[2].SetActive(false);
                players[3].SetActive(true);
                _selPlayer = players[3];
                //			cars [4].SetActive(false);
                break;
        }
    }


    private void Fail()
    {
        gameOverCanv.SetActive(true);
        AudioListener.volume = 0f;

        if (_gOver)
        {
            // if (cour != null) StopCoroutine(cour);


            _gOver = false;
        }
    }


    public void Fail2()
    {
        // if (cour != null) StopCoroutine(cour);
        gameOverCanv.SetActive(true);
        AudioListener.volume = 0f;
    }

    #region BUTTONS

    private bool _steering;

    public void StopPanel()
    {
        stopCanv.SetActive(true);
        Time.timeScale = 0;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        isPaused = !hasFocus;
        if (!hasFocus) 
            StopPanel();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;
        if(pauseStatus) 
            StopPanel();
    }


    public void BtnDown()
    {
        _selPlayer.GetComponent<Rigidbody>().drag = 1.15f;
    }

    public void BtnUp()
    {
        _selPlayer.GetComponent<Rigidbody>().drag = 0.05f;
    }


    public void OnMenu()
    {
        GroundFall.AdmirationCount = 0;
        //if (PlayerPrefs.GetInt ("ADSUNLOCK") != 1) 

        LoadLevels.LVar = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void OnRefresh()
    {
        GroundFall.AdmirationCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        gameOverCanv.SetActive(false);
    }

    public void OnGo()
    {
        GroundFall.AdmirationCount = 0;

        PlayerPrefs.SetInt("LevelNumber", PlayerPrefs.GetInt("LevelNumber") + 1);

        Debug.Log("PLayerPrefsComplete");
        var currentLevel = SceneManager.GetActiveScene().buildIndex;

        if (currentLevel < 13)
        {
            SceneManager.LoadScene(currentLevel + 1);
        }
        else
        {
            SceneManager.LoadScene(1);
            AllMenuManager.IsPitchP = true;
        }
    }

    public void OnGo2()
    {
        GroundFall.AdmirationCount = 0;

        PlayerPrefs.SetInt("Mode2LevelNumber", PlayerPrefs.GetInt("Mode2LevelNumber") + 1);

        var currentLevel = SceneManager.GetActiveScene().buildIndex;

        if (currentLevel < 24)
        {
            SceneManager.LoadScene(currentLevel + 1);
        }
        else
        {
            SceneManager.LoadScene(1);
            AllMenuManager.IsPitchP = true;
        }
    }

    public void OnLater()
    {
        // if (cour != null) StopCoroutine(cour);


        // isPause = false;
        AudioListener.volume = PlayerPrefs.GetFloat("Sound");


        Time.timeScale = 1;
        if (_steering) _steering = false;

        stopCanv.SetActive(false);
    }


    public void OnAdministration()
    {
        UpdateController(2);
        _control.SetMobileController(2);
        PlayerPrefs.SetInt("Controls", 2);
    }

    public void OnSpeed()
    {
        UpdateController(1);

        PlayerPrefs.SetInt("Controls", 1);

        _control.SetMobileController(1);
    }

    public void OnBtn()
    {
        UpdateController(0);
        PlayerPrefs.SetInt("Controls", 0);

        _control.SetMobileController(0);
    }

    private void UpdateController(int val)
    {
        if (val == 0)
        {
            frameIcon[0].SetActive(true);
            frameIcon[1].SetActive(false);
            frameIcon[2].SetActive(false);
        }
        else if (val == 1)
        {
            frameIcon[0].SetActive(false);
            frameIcon[1].SetActive(true);
            frameIcon[2].SetActive(false);
        }
        else if (val == 2)
        {
            frameIcon[0].SetActive(false);
            frameIcon[1].SetActive(false);
            frameIcon[2].SetActive(true);
        }
    }

    #endregion


    #region TIMER

    private void TimerBegin(float from)
    {
        _isEnd = false;
        EndTimer = from;
        StartCoroutine(TextChangeIe());
    }

    private void Update()
    {
        if (IsTime) EndTimer -= Time.deltaTime;

        _min = Mathf.Floor(EndTimer / 60);
        _sec = EndTimer % 60;
        if (_sec > 59) _sec = 59;

        if (_min < 0)
        {
            _isEnd = true;
            _min = 0;
            _sec = 0;
            Fail();
        }
    }

    private IEnumerator TextChangeIe()
    {
        while (!_isEnd)
        {
            countdownText.text = $"{_min:0}:{_sec:00}";
            yield return new WaitForSeconds(0.2f);
        }
    }

    #endregion
}