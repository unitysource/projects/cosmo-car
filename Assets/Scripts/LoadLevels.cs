﻿using System.Collections;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour
{
    public static int LVar;
    [SerializeField] [Scene] private string[] easyLevelScenes; // 5 easy levels
    [SerializeField] [Scene] private string[] hardLevelScenes; // 5 hard levels
    // private int _n = 0;

    private void Start()
    {
        Time.timeScale = 1;
        AudioListener.volume = PlayerPrefs.GetFloat("Sound");
        StartCoroutine(IsLoadWaiting());
    }

    private IEnumerator IsLoadWaiting()
    {
        yield return new WaitForSeconds(1f);
        if (LVar == 1) SceneManager.LoadSceneAsync("MainMenu");

        if (LVar == 0)
        {
            var level = PlayerPrefs.GetInt("LevelNumber");
            var mode2Level = PlayerPrefs.GetInt("Mode2LevelNumber");

            for (int i = 0; i < easyLevelScenes.Length; i++)
            {
                if (level == i + 1)
                {
                    Debug.Log(i);
                    SceneManager.LoadSceneAsync(easyLevelScenes[i]);
                    yield break;
                }
            }
                
            for (int i = 0; i < hardLevelScenes.Length; i++)
            {
                if (mode2Level == i + 1)
                {
                    Debug.Log(i);
                    SceneManager.LoadSceneAsync(hardLevelScenes[i]);
                    yield break;
                }
            }
        }
    }
}