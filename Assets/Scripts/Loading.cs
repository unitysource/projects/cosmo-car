﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    private void Start()
    {
        Invoke(nameof(LoadScene), 3f);
    }


    private void LoadScene()
    {
        SceneManager.LoadScene("Daily Rewards Landscape");
    }
}