﻿using Tools;
using UnityEngine;
using UnityEngine.Serialization;

public class PickupMoney : MonoBehaviour
{
public AudioSource audioS;
 public AudioClip audioMoney;


    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag(Tags.Hero))
        {
            EndGame.MoneyGiveUp++;
            audioS.GetComponent<AudioSource>().PlayOneShot(audioMoney);
            gameObject.SetActive(false);
        }
    }
}