﻿using UnityEngine;
using UnityEngine.Serialization;

public class Pilot : MonoBehaviour
{
     public float move;
    public bool isDamage;

    private void Update()
    {
        if (gameObject.tag == "HeliCallTag")
            if (isDamage)
                transform.Translate(Vector3.forward * (move * Time.deltaTime));

        if (gameObject.tag == "Stem")
        {
            print("here");

            if (isDamage) gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }

        if (gameObject.tag == "Respawn")
            if (isDamage)
                transform.Translate(Vector3.forward * (move * Time.deltaTime));

        if (gameObject.tag == "heliismovetoup")
            if (isDamage)
                transform.Translate(Vector3.up * (move * Time.deltaTime));

        if (gameObject.tag == "EnemyCar")
            if (isDamage)
                transform.Translate(Vector3.forward * (move * Time.deltaTime));

        if (gameObject.tag == "VanilaCar")
            if (isDamage)
                transform.Translate(Vector3.forward * (move * Time.deltaTime));

        if (gameObject.CompareTag("HeliCallTag1"))			
            if (isDamage)
                transform.Translate(Vector3.forward * (move * Time.deltaTime));
    }
}