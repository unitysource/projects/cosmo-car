﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SelectCarManager : MonoBehaviour
{
    public static int SelPlayers;
    public static int scrInt;

    public Text salaryMy;

    public int[] carSalary;
    public GameObject[] carAccess;

    public GameObject selCarUi;

    public GameObject[] featuresPlayer;

    public GameObject lockedPlayers;

    public GameObject poorCostPanel;

    public Button[] selB;
    public Text earnCost;

    public Text text;

    public GameObject[] locks;

    public GameObject goBtn;
    public GameObject buyBtn;
    public GameObject cost;
    private int _characteristic;
    private int _nonCar;

    private int _players;
    private int _result;

    private void Start()
    {
        PlayerPrefs.SetInt("Seconds", 0);
        PlayerPrefs.SetInt("Minutes", 0);


        salaryMy.text = PlayerPrefs.GetInt("Earnings").ToString();
        _players = 0;
        _characteristic = 0;
        _nonCar = 0;
    }

    private void Update() =>
        salaryMy.text = PlayerPrefs.GetInt("Earnings").ToString();

    public void NextPlayer()
    {
        carAccess[_players].SetActive(false);
        featuresPlayer[_players].SetActive(false);
        lockedPlayers.SetActive(false);

        if (_players == 3 && _characteristic == 3)
        {
            _players = 0;
            _characteristic = 0;
            lockedPlayers.SetActive(false);
        }
        else
        {
            _players++;
            _characteristic++;
        }


        switch (_players)
        {
            case 0:
                lockedPlayers.SetActive(false);
                OnGoOrBuy(true);

                break;
            case 1:
                if (PlayerPrefs.GetInt("car1") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 2:
                if (PlayerPrefs.GetInt("car2") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 3:
                if (PlayerPrefs.GetInt("car3") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 4:
                if (PlayerPrefs.GetInt("car4") == 1)
                    lockedPlayers.SetActive(false);
                else
                    lockedPlayers.SetActive(true);

                break;
//		case 5:
//			if (PlayerPrefs.GetInt ("car5") == 1)
//				carLock.SetActive (false);
//			else
//				carLock.SetActive (true);
//
//			break;
        }


        carAccess[_players].SetActive(true);
        featuresPlayer[_characteristic].SetActive(true);
    }

    public void PreviewPlayer()
    {
        carAccess[_players].SetActive(false);
        featuresPlayer[_players].SetActive(false);
        lockedPlayers.SetActive(false);

        _players--;
        _characteristic--;

        if (_players < 0 && _characteristic < 0)
        {
            _players = 3;
            _characteristic = 3;
        }


        switch (_players)
        {
            case 0:
                lockedPlayers.SetActive(false);
                OnGoOrBuy(true);

                break;
            case 1:
                if (PlayerPrefs.GetInt("car1") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 2:
                if (PlayerPrefs.GetInt("car2") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 3:
                if (PlayerPrefs.GetInt("car3") == 1)
                {
                    lockedPlayers.SetActive(false);
                    OnGoOrBuy(true);
                }

                else
                {
                    lockedPlayers.SetActive(true);
                    OnGoOrBuy(false);
                }


                break;
            case 4:
                if (PlayerPrefs.GetInt("car4") == 1)
                    lockedPlayers.SetActive(false);
                else
                    lockedPlayers.SetActive(true);

                break;
            //		case 5:
            //			if (PlayerPrefs.GetInt ("car5") == 1)
            //				carLock.SetActive (false);
            //			else
            //				carLock.SetActive (true);
            //
            //			break;
        }


        carAccess[_players].SetActive(true);
        featuresPlayer[_characteristic].SetActive(true);
    }

    public void SelectPlayer()
    {
        Time.timeScale = 1f;
        selCarUi.SetActive(true);
        poorCostPanel.SetActive(false);
    }

    public void OnGo()
    {
        SelPlayers = _players;
        LoadLevels.LVar = 0;
        SceneManager.LoadSceneAsync("LoadingScene");
    }

    public void OnSelect()
    {
        SelPlayers = _players;

        if (SelPlayers == 0)
        {
            LoadLevels.LVar = 0;
            SceneManager.LoadSceneAsync("LoadingScene");
        }

        if (SelPlayers == 1)
        {
            if (PlayerPrefs.GetInt("car1") == 1)
            {
                LoadLevels.LVar = 0;
                SceneManager.LoadSceneAsync("LoadingScene");
            }
            else
            {
                if (PlayerPrefs.GetInt("Earnings") >= carSalary[1])
                {
                    PlayerPrefs.SetInt("Earnings", PlayerPrefs.GetInt("Earnings") - carSalary[1]);
                    PlayerPrefs.SetInt("car1", 1);

                    cost.SetActive(false);
                    lockedPlayers.SetActive(false);
                    buyBtn.SetActive(false);
                    goBtn.SetActive(true);

                    //LoadingScreen.loadingVariable = 0;
                    //SceneManager.LoadSceneAsync ("LoadingScene");
                }
                else
                {
                    selCarUi.SetActive(false);
                    poorCostPanel.SetActive(true);
                    _result = carSalary[1] - PlayerPrefs.GetInt("Earnings");
                    text.text = _result.ToString();
                    earnCost.text = PlayerPrefs.GetInt("Earnings").ToString();
                }
            }
        }

        if (SelPlayers == 2)
        {
            if (PlayerPrefs.GetInt("car2") == 1)
            {
                LoadLevels.LVar = 0;
                SceneManager.LoadSceneAsync("LoadingScene");
            }
            else
            {
                if (PlayerPrefs.GetInt("Earnings") >= carSalary[2])
                {
                    PlayerPrefs.SetInt("Earnings", PlayerPrefs.GetInt("Earnings") - carSalary[2]);
                    PlayerPrefs.SetInt("car2", 1);

                    cost.SetActive(false);
                    lockedPlayers.SetActive(false);
                    buyBtn.SetActive(false);
                    goBtn.SetActive(true);
                    //LoadingScreen.loadingVariable = 0;
                    //SceneManager.LoadSceneAsync ("LoadingScene");
                }
                else
                {
                    selCarUi.SetActive(false);
                    poorCostPanel.SetActive(true);
                    _result = carSalary[2] - PlayerPrefs.GetInt("Earnings");
                    text.text = _result.ToString();
                    earnCost.text = PlayerPrefs.GetInt("Earnings").ToString();
                }
            }
        }

        if (SelPlayers == 3)
        {
            if (PlayerPrefs.GetInt("car3") == 1)
            {
                LoadLevels.LVar = 0;
                SceneManager.LoadSceneAsync("LoadingScene");
            }
            else
            {
                if (PlayerPrefs.GetInt("Earnings") >= carSalary[3])
                {
                    PlayerPrefs.SetInt("Earnings", PlayerPrefs.GetInt("Earnings") - carSalary[3]);
                    PlayerPrefs.SetInt("car3", 1);

                    cost.SetActive(false);
                    lockedPlayers.SetActive(false);
                    buyBtn.SetActive(false);
                    goBtn.SetActive(true);
                    //LoadingScreen.loadingVariable = 0;
                    //SceneManager.LoadSceneAsync ("LoadingScene");
                }
                else
                {
                    selCarUi.SetActive(false);
                    poorCostPanel.SetActive(true);
                    _result = carSalary[3] - PlayerPrefs.GetInt("Earnings");
                    text.text = _result.ToString();
                    earnCost.text = PlayerPrefs.GetInt("Earnings").ToString();
                }
            }
        }

        if (SelPlayers == 4)
        {
            if (PlayerPrefs.GetInt("car4") == 1)
            {
                LoadLevels.LVar = 0;
                SceneManager.LoadSceneAsync("LoadingScene");
            }
            else
            {
                if (PlayerPrefs.GetInt("Earnings") >= carSalary[4])
                {
                    PlayerPrefs.SetInt("Earnings", PlayerPrefs.GetInt("Earnings") - carSalary[4]);
                    PlayerPrefs.SetInt("car4", 1);

                    cost.SetActive(false);
                    lockedPlayers.SetActive(false);
                    buyBtn.SetActive(false);
                    goBtn.SetActive(true);
                    //LoadingScreen.loadingVariable = 0;
                    //SceneManager.LoadSceneAsync ("LoadingScene");
                }
                else
                {
                    selCarUi.SetActive(false);
                    poorCostPanel.SetActive(true);
                    _result = carSalary[4] - PlayerPrefs.GetInt("Earnings");
                    text.text = _result.ToString();
                    earnCost.text = PlayerPrefs.GetInt("Earnings").ToString();
                }
            }
        }
    }


    public void OnCross()
    {
        lockedPlayers.SetActive(true);
        poorCostPanel.SetActive(false);
    }

    public void OnBack()
    {
        if (AllMenuManager.Pitch) AllMenuManager.IsPitchP = true;
        if (AllMenuManager.Pitch == false) AllMenuManager.IsPitchP2 = true;
        SceneManager.LoadScene(1);
    }


    private void OnGoOrBuy(bool playing)
    {
        if (playing)
        {
            goBtn.SetActive(true);
            buyBtn.SetActive(false);
            cost.SetActive(false);
        }
        else
        {
            goBtn.SetActive(false);
            buyBtn.SetActive(true);
            cost.SetActive(true);
            cost.GetComponentInChildren<Text>().text = carSalary[_players].ToString();
        }
    }
}