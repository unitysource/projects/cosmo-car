﻿using Tools;
using UnityEngine;
using UnityEngine.Serialization;

public class SpawnSavePoint : MonoBehaviour
{
    // public static int DoteForSpawn = 0;
public Animator saveAdore;
    private static readonly int SpawnPoint = Animator.StringToHash("SpawnPoint");

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag(Tags.Hero))
        {
            GroundFall.SpawnCount++;
            gameObject.SetActive(false);
            saveAdore.SetTrigger(SpawnPoint);
        }
    }
}