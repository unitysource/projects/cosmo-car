// using System.Collections.Generic;
// using System.IO;
// using System.Linq;
// using System.Runtime.Serialization.Formatters.Binary;
// // using Newtonsoft.Json;
// using UnityEditor;
// using UnityEditor.SceneManagement;
// using UnityEngine;
// using UnityEngine.SceneManagement;
//
// namespace _Scripts.Tools.Editor
// {
//     public class CleverRenameWindow : EditorWindow
//     {
//         // private readonly Dictionary<string, string> _namesHistory = new Dictionary<string, string>();
//         private Dictionary<int, string> _namesHistory = new Dictionary<int, string>();
//
//         private bool _changeAllRandom;
//         private bool _changeInAllScenes = true;
//         private string _json;
//
//         private string _newName = "New";
//         private string _oldName = "Old";
//         private bool _stringGroupEnabled = true;
//
//         private void OnGUI()
//         {
//             GUILayout.Label("GENERAL RENAME SETTINGS", EditorStyles.boldLabel);
//             GUILayout.Space(10f);
//
//             _changeInAllScenes = EditorGUILayout.Toggle("Change In All Scenes", _changeInAllScenes);
//             _changeAllRandom = EditorGUILayout.Toggle("Change All objects randomly", _changeAllRandom);
//
//             _stringGroupEnabled = EditorGUILayout.BeginToggleGroup("Change By Name", _stringGroupEnabled);
//             // _oldName = EditorGUILayout.TextField("Old Name", _oldName);
//             // _newName = EditorGUILayout.TextField("New Name", _newName);
//             EditorGUILayout.EndToggleGroup();
//
//
//             if (GUILayout.Button("Rename"))
//             {
//                 Debug.Log(_namesHistory.Count);
//                 // if (_stringGroupEnabled)
//                 // {
//                 if (_changeInAllScenes)
//                 {
//                     var activeSceneName = SceneManager.GetActiveScene().path;
//                     var sceneNumber = SceneManager.sceneCountInBuildSettings;
//                     var scenes = new string[sceneNumber];
//
//                     for (var i = 0; i < sceneNumber; i++)
//                         scenes[i] = Path.GetFullPath(SceneUtility.GetScenePathByBuildIndex(i));
//
//                     foreach (var sceneStr in scenes)
//                     {
//                         EditorSceneManager.OpenScene(sceneStr);
//                         ChangeNamesInInspector(false);
//                         var scene = SceneManager.GetActiveScene();
//                         EditorSceneManager.MarkSceneDirty(scene);
//                         AssetDatabase.SaveAssets();
//                         EditorSceneManager.SaveScene(scene, "", false);
//                     }
//
//                     EditorSceneManager.OpenScene(activeSceneName);
//                 }
//                 else
//                 {
//                     ChangeNamesInInspector(false);
//                 }
//                 // }
//             }
//             else if (GUILayout.Button("Reverse Names"))
//             {
//                 var oldName = _oldName;
//                 _oldName = EditorGUILayout.TextField("Old Name", _newName);
//                 _newName = EditorGUILayout.TextField("New Name", oldName);
//             }
//
//             else if (GUILayout.Button("Return All Data"))
//             {
//                 Debug.Log(_namesHistory.Count);
//                 if (_changeInAllScenes)
//                 {
//                     var activeSceneName = SceneManager.GetActiveScene().path;
//                     var sceneNumber = SceneManager.sceneCountInBuildSettings;
//                     var scenes = new string[sceneNumber];
//
//                     for (var i = 0; i < sceneNumber; i++)
//                         scenes[i] = Path.GetFullPath(SceneUtility.GetScenePathByBuildIndex(i));
//
//                     foreach (var sceneStr in scenes)
//                     {
//                         EditorSceneManager.OpenScene(sceneStr);
//                         ChangeNamesInInspector(true);
//                         var scene = SceneManager.GetActiveScene();
//                         EditorSceneManager.MarkSceneDirty(scene);
//                         AssetDatabase.SaveAssets();
//                         EditorSceneManager.SaveScene(scene, "", false);
//                     }
//
//                     EditorSceneManager.OpenScene(activeSceneName);
//                 }
//                 else
//                 {
//                     ChangeNamesInInspector(true);
//                 }
//             }
//         }
//         // private bool _changeInCode = true;
//
//         [MenuItem("Tools/Clever Rename", false, 1)]
//         public static void OpenCleverRename()
//         {
//             GetWindow<CleverRenameWindow>("Clever Rename");
//         }
//
//         private void ChangeNamesInInspector(bool isReturnData)
//         {
//             var objects = _changeAllRandom
//                 ? GetAllObjectsOnlyInScene().ToArray()
//                 : GetAllObjectsOnlyInScene().Where(obj => obj.name == _oldName).ToArray();
//
//             if (objects.Length > 0)
//             {
//                 // Debug.Log($"Confirm. All GameObjects with name: [{_oldName}] changing to name: [{_newName}]");
//                 
//                 if (_namesHistory.Count == 0)
//                 {
//                     foreach (var gameObject in objects)
//                         if (!_namesHistory.ContainsKey(gameObject.GetHashCode()))
//                             _namesHistory.Add(gameObject.GetHashCode(), gameObject.name);
//                     // _json = JsonConvert.SerializeObject(_namesHistory, Formatting.Indented);
//                     // SaveFile();
//                 }
//
//                 _namesHistory = LoadFile();
//                 Debug.Log(Application.persistentDataPath);
//
//                 foreach (var gameObject in objects)
//                 {
//                     Undo.RecordObject(gameObject, "Rename");
//                     if (isReturnData)
//                     {
//                         var hashCode = gameObject.GetHashCode();
//                         if (_namesHistory.ContainsKey(hashCode))
//                         {
//                             gameObject.name = _namesHistory[hashCode];
//                             // _namesHistory.Remove(hashCode);
//                         }
//                     }
//                     else
//                     {
//                         if (_changeAllRandom)
//                         {
//                             var randLength = Random.Range(5, 11);
//                             _oldName = gameObject.name;
//                             _newName = GenerateName(randLength);
//                             gameObject.name = _newName;
//
//                             if (_namesHistory.ContainsKey(gameObject.GetHashCode()))
//                                 _namesHistory[gameObject.GetHashCode()] = _namesHistory[gameObject.GetHashCode()];
//                         }
//                         else
//                         {
//                             gameObject.name = _newName;
//                         }
//                     }
//
//                     if (AssetDatabase.GetAssetPath(gameObject) != null)
//                         AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(gameObject), gameObject.name);
//                     // foreach (var pair in _namesHistory)
//                     // Debug.Log($"old: {pair.Key}, new: {pair.Value}");
//                 }
//             }
//             else
//             {
//                 Debug.LogError($"GameObject with name: [{_oldName}] in this Scene isn't find!");
//             }
//         }
//
//         private static string GenerateName(int len)
//         {
//             var r = new System.Random();
//             var consonants = new[]
//             {
//                 "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l",
//                 "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x"
//             };
//             var vowels = new[] {"a", "e", "i", "o", "u", "ae", "y"};
//             var newName = "";
//             newName = $"{newName}{consonants[r.Next(consonants.Length)].ToUpper()}";
//             newName = $"{newName}{vowels[r.Next(vowels.Length)]}";
//             var b = 2; //b tells how many times a new letter has been added.
//             //It's 2 right now because the first two letters are already in the name.
//             while (b < len)
//             {
//                 newName = $"{newName}{consonants[r.Next(consonants.Length)]}";
//                 b++;
//                 newName = $"{newName}{vowels[r.Next(vowels.Length)]}";
//                 b++;
//             }
//
//             return newName;
//         }
//
//         /// <summary>
//         ///     Get All Prefabs
//         /// </summary>
//         /// <returns></returns>
//         private static List<GameObject> GetNonSceneObjects()
//         {
//             var objectsInScene = new List<GameObject>();
//
//             foreach (var go in (GameObject[]) Resources.FindObjectsOfTypeAll(typeof(GameObject)))
//                 if (EditorUtility.IsPersistent(go.transform.root.gameObject) &&
//                     !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
//                     objectsInScene.Add(go);
//
//             return objectsInScene;
//         }
//
//
//         private static List<GameObject> GetAllObjectsOnlyInScene()
//         {
//             var objectsInScene = new List<GameObject>();
//
//             foreach (var go in (GameObject[]) Resources.FindObjectsOfTypeAll(typeof(GameObject)))
//                 if (!EditorUtility.IsPersistent(go.transform.root.gameObject) &&
//                     !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
//                     objectsInScene.Add(go);
//
//             return objectsInScene;
//         }
//
//         public void SaveFile()
//         {
//             var destination = $"{Application.persistentDataPath}/save.dat";
//
//             var file = File.Exists(destination) ? File.OpenWrite(destination) : File.Create(destination);
//
//             var bf = new BinaryFormatter();
//             bf.Serialize(file, _namesHistory);
//             file.Close();
//         }
//
//         public Dictionary<int, string> LoadFile()
//         {
//             // var destination = $"{Application.persistentDataPath}/save.dat";
//             // FileStream file;
//             //
//             // if (File.Exists(destination))
//             // {
//             //     file = File.OpenRead(destination);
//             // }
//             // else
//             // {
//             //     Debug.LogError("File not found");
//             //     return null;
//             // }
//             //
//             // var bf = new BinaryFormatter();
//             // string data = (string) bf.Deserialize(file);
//             // file.Close();
//             // var dict = JsonConvert.DeserializeObject<Dictionary<int, string>>(data);
//             
//             string destination = $"{Application.persistentDataPath}/save.dat";
//             FileStream file;
//  
//             if(File.Exists(destination)) file = File.OpenRead(destination);
//             else
//             {
//                 Debug.LogError("File not found");
//                 return null;
//             }
//  
//             BinaryFormatter bf = new BinaryFormatter();
//             var data = (Dictionary<int, string>) bf.Deserialize(file);
//             file.Close();
//             return data;
//         }
//     }
// }
