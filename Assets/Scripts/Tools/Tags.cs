namespace Tools
{
    public static class Tags
    {
        public const string PlaneCountLives = "PlaneCountLives";
        public const string Hero = "Hero";
        public const string SphereColl = "SphereColl";
        public const string WallOfSphereColl = "WallOfSphereColl";
    }
}