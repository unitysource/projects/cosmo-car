﻿using UnityEngine;
using UnityEngine.Serialization;

public class TrigDetect : MonoBehaviour
{
    public GameObject prePlayer;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Hero"))
        {
            prePlayer.GetComponent<Pilot>().enabled = true;
            prePlayer.GetComponent<Pilot>().isDamage = true;
        }
    }
}