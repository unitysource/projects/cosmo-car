﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class TriggerDetectMain : MonoBehaviour
{
    private static int _scenesCount;
public Transform spawnTransform;

    public GameObject pointObj;

    public GameObject[] bars;

    private Pilot _pilot;

    public void Start()
    {
        _scenesCount = SceneManager.GetActiveScene().buildIndex;
    }

    public void OnTriggerEnter(Collider collider)
    {
        switch (_scenesCount)
        {
            case 4:
            {
                if (gameObject.tag == "Collider")
                    Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                break;
            }
            case 5:
                break; // ending if (SceneNumber.Equals (2) statement
            case 6:
                break; // ending if (SceneNumber.Equals (3) statement
            case 7:
                break; // ending if (SceneNumber.Equals (4) statement
            case 8:
            {
                if (gameObject.tag == "HeliCallTag")
                {
                    Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                    _pilot = pointObj.GetComponent<Pilot>();
                    _pilot.isDamage = true;
                    //		StartCoroutine (DriveObjects ());
                    //Destroy (this.WhatToSpawnPrefab, 10);
                }

                if (gameObject.tag == "OpenTheDoorColl1")
                {
                    //_animator.SetBool ("open", true);
                    Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                    StartCoroutine(DriverObj());
                    //	Destroy (this.WhatToSpawnPrefab, 10);
                }

                if (gameObject.tag == "OpenTheDoorColl2")
                {
                    //_animator.SetBool ("open", true);
                }

                if (gameObject.tag == "ColliderForEnemyCar")
                {
                    pointObj = GameObject.FindWithTag("EnemyCar");
                    _pilot = pointObj.GetComponent<Pilot>();
                    _pilot.isDamage = true;
                    StartCoroutine(DriverObj());
                    //Destroy (this.WhatToSpawnPrefab, 10);
                }

                if (gameObject.tag == "StemCollider")
                {
                    Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                    bars[0].GetComponent<Rigidbody>().isKinematic = false;
                    bars[1].GetComponent<Rigidbody>().isKinematic = false;
                    bars[2].GetComponent<Rigidbody>().isKinematic = false;
                    StartCoroutine(DriverObj());
//				Destroy (this.Barrels[0], 10);
//				Destroy (this.Barrels[1], 10);
//				Destroy (this.Barrels[2], 10);
                }

                if (gameObject.tag == "SecondEnemyCarCollider")
                {
                    pointObj = GameObject.FindWithTag("VanilaCar");
                    _pilot = pointObj.GetComponent<Pilot>();
                    _pilot.isDamage = true;
                    StartCoroutine(DriverObj());
                    //	Destroy (this.WhatToSpawnPrefab, 3);
                }


                if (gameObject.tag == "HeliCallTag1")
                {
                    pointObj = GameObject.FindWithTag("Respawn");
                    _pilot = pointObj.GetComponent<Pilot>();
                    _pilot.isDamage = true;
                    StartCoroutine(DriverObj());
                    //	Destroy (this.WhatToSpawnPrefab, 10);
                } // ending if (SceneNumber.Equals (5) statement

                break;
            }
        }


        if (_scenesCount.Equals(9))
        {
            Debug.Log("Scene 9");
            if (gameObject.tag == "StemCollider")
            {
                pointObj.GetComponent<Pilot>().isDamage = false;
                Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                bars[0].GetComponent<Rigidbody>().isKinematic = false;
                bars[1].GetComponent<Rigidbody>().isKinematic = false;
                bars[2].GetComponent<Rigidbody>().isKinematic = false;
                StartCoroutine(DriverObj());
            }

            if (gameObject.tag == "ColliderForEnemyCar")
            {
                pointObj = GameObject.FindWithTag("EnemyCar");
                _pilot = pointObj.GetComponent<Pilot>();
                _pilot.isDamage = true;
                StartCoroutine(DriverObj());
                //Destroy (this.WhatToSpawnPrefab, 10);
            }


            if (gameObject.tag == "Respawn")
            {
                pointObj.GetComponent<Pilot>().isDamage = false;
                Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                bars[0].GetComponent<Rigidbody>().isKinematic = false;
                bars[1].GetComponent<Rigidbody>().isKinematic = false;
                bars[2].GetComponent<Rigidbody>().isKinematic = false;
                StartCoroutine(DriverObj());
            }

            if (gameObject.tag == "SecondEnemyCarCollider")
            {
                pointObj = GameObject.FindWithTag("VanilaCar");
                _pilot = pointObj.GetComponent<Pilot>();
                _pilot.isDamage = true;
                StartCoroutine(DriverObj());
                //	Destroy (this.WhatToSpawnPrefab, 3);
            }
        } // ending if (SceneNumber.Equals (6) statement


        if (_scenesCount.Equals(10))
        {
            //if (gameObject.tag == "DoorOpenCollider1") {
            //	_animator.SetBool ("open", true);

            //}


            if (gameObject.tag == "ColliderForEnemyCar")
            {
                pointObj = GameObject.FindGameObjectWithTag("HeliCallTag");
                _pilot = pointObj.GetComponent<Pilot>();
                _pilot.isDamage = true;
                StartCoroutine(DriverObj());
                //	Destroy (this.WhatToSpawnPrefab, 10);
            }

            if (gameObject.tag == "SecondEnemyCarCollider")
            {
                pointObj = GameObject.FindGameObjectWithTag("HeliIsMoveToUp");
                _pilot = pointObj.GetComponent<Pilot>();
                _pilot.isDamage = true;
                StartCoroutine(DriverObj());
                //		Destroy (this.WhatToSpawnPrefab, 20);
            }
        } // ending if (SceneNumber.Equals (7) statement


        if (_scenesCount.Equals(11))
        {
            if (gameObject.tag == "OpenTheDoorColl1")
            {
                //_animator.SetBool ("open", true);
                pointObj.GetComponent<Pilot>().isDamage = true;
                Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);

//				if (once) {
//					
//					Destroy (this.WhatToSpawnPrefab, 10);
//					once = false;
//				}
//					
////				StartCoroutine (DriveObjects ());
//
            } // ending if (SceneNumber.Equals (8) statement

            if (gameObject.tag == "OpenTheDoorColl2")
            {
                //_animator.SetBool ("open", true);
                // IsOnce = true;
            } // ending if (SceneNumber.Equals (8) statement


            if (gameObject.tag == "HeliCallTag")
            {
                Instantiate(pointObj, spawnTransform.position, spawnTransform.rotation);
                _pilot = pointObj.GetComponent<Pilot>();
                _pilot.isDamage = true;
                StartCoroutine(DriverObj());
            } // ending if (SceneNumber.Equals (8) statement

            if (gameObject.tag == "FinishedCall")
            {
                //Application.LoadLevel ("level9");
            }
        } // ending if (SceneNumber.Equals (8) statement

        switch (_scenesCount)
        {
            case 12:
            {
                if (gameObject.tag == "FinishedCall") Resources.UnloadUnusedAssets();
                //	Application.LoadLevel ("level10");
                // ending if (SceneNumber.Equals (9) statement

                break;
            }
            case 13:
            {
                if (gameObject.tag == "FinishedCall")
                    Resources.UnloadUnusedAssets();
                // ending if (SceneNumber.Equals (6) statement

                break;
            }
        }
    }
    // void OnTriggerEnter function ended here

    // IEnumerator installDriver ()
    // {
    // 	yield return new WaitForSeconds (0.5f);
    // 	_pilot.isDamage = true;
    // 		
    //
    // }

    private IEnumerator DriverObj()
    {
        yield return new WaitForSeconds(5);

        Destroy(gameObject);
    }
}
// class paranthesis ended here