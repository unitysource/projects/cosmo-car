# Cosmo Car

![Game Logo](/images/logo.png)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)

## About the Game

Create car and drive in space

## Gameplay

Drive car and collect coins. Avoid obstacles and reach the finish line. Collect coins to unlock new cars.

![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:
- 3D graphics
- Car customization

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/cosmo-car.git`
2. Navigate to the game directory: `cd cosmo-car`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only desktop controls are available at the moment.

**Desktop Controls:**
- WASD or Arrow keys to move

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
